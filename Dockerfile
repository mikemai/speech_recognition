FROM python:3.6-slim

RUN apt-get update && apt-get install -qq -y \
  build-essential libpq-dev \
  libsndfile1-dev --no-install-recommends

RUN mkdir -p /speech_proj /root/.jupyter

COPY .jupyter /root/.jupyter
RUN pip install --no-cache-dir -r /root/.jupyter/requirements.txt

WORKDIR /speech_proj
CMD jupyter notebook